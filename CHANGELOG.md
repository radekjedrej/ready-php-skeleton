
# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

For more information about keeping good change logs please refer to [keep a changelog](https://github.com/olivierlacan/keep-a-changelog).

### ADDED
- CHANGELOG.MD

### CHANGED
- How footer is checking if social networks have a link

### FIXED
- A bug that was super annoying


## [0.1.4] - 2019-1-16
### CHANGED
 - Updating FEBP to clone down `master` branch


## [0.1.3] - 2019-1-11
### ADDED
 - Gitignore frontend boilerplate files


## [0.1.2] - 2019-1-11
### FIXED
 - Getting master branch of `ready-frontend-boilerplate`


## [0.1.1] - 2019-1-11
### FIXED
 - Building of the project


## [0.1.0] - 2019-1-11
### ADDED
 - Initial release. Build task currently needs some work but for local development the boilerplate is there
