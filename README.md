# Ready - PHP Skeleton

## Setup
This project sets itself up using a bash script. For a basic guide to bash script read (http://matt.might.net/articles/bash-by-example/)[this guide].

1. You just need to run this command from the root of the project `./.php-skeleton-setup`. This will install all your dependancies

2. You will need to update `config/webpack.dev` server information with your local server. The line you'll need to change is;
```
target: 'http://ready-febp.local:81',
```

To your have your servers host name and port.
```
target: 'http://your-local-server:your-port',
```        

3. Create a `.env` file. There is a `.env.example` file you can use as a template for your project
