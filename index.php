<?php 
    include_once 'partials/core/boot.php'; 
    include_once 'lib/form/capture.php'; 

    $form_data      = ( !empty($form_data) ) ? $form_data : [];
    $form_err       = ( !empty($form_err) ) ? $form_err : [];

    // var_dump($form_err);

?>


<!DOCTYPE html>
<html lang="en-GB">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <?php include_once 'partials/core/head.php'; ?>
    </head>
    <body>
        <?= Utils\nb_load_template_part('partials/core/header', [
            'heading' => 'Ready Frontend Boiletplate'
        ]); ?>


        <main id="main-content" class="main">

            <section class=" container band">
                <h2 class="heading--bravo">How to the boilerplate</h2>

                <ol>
                    <li>This is a list of item</li>
                </ol>
            </section>

            <section class="container">

                <?php
                echo Utils\nb_load_template_part('partials/components/form/wrapper', array(
                    'form_data'     => $form_data,
                    'form_err'      => $form_err
                )); ?>

            </section>

        </main>


        <?php include_once 'partials/core/javascript-assets.php'; ?>
        <?php include_once 'partials/core/google-analytics.php'; ?>
    </body>
</html>
