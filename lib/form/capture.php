<?php
    require __DIR__ . "/../../vendor/autoload.php";
    include_once __DIR__ .'/init.php';


    if($_SERVER['REQUEST_METHOD'] == 'POST') {

        // if($contentType === "application/json") {
        // Process form
        // Sanitaze POST data
        $content = trim(file_get_contents("php://input"));
        $safe_content = filter_var($content);
        $decoded = json_decode($safe_content, true);

        // Ternary operators
        $format = ( !empty ($decoded) ) ? $decoded : $_POST;
        $brackets = ( !empty ($decoded) ) ? 'multiple_checkboxes[]' : 'multiple_checkboxes';

        /**
         * Use this piece of code if you have arrays with multiple choices selection. Which have
         * to be saved in the array
         */

        // Initialize empty arrays for multiple_checkboxes
        $multiple_checkboxes = [];

        // Validate arrays and assign to a variable
        if (isset($format[$brackets])) {
            $multiple_checkboxes = $format[$brackets];
        }

        /**
         * Initiate our form arrays, first we create empty array for our errors, and one for our incoming data. We trim our all incoming data except arrays, because that will gave us "NULL".
         */

        // Init errors data
        $form_err = [
            'warning' => 'warning',
            'success' => false
        ];
        // Init data
        $form_data = [
            'first_name'          => trim($format['first_name']),
            'email'               => trim($format['email']),
            'select_option'       => trim($format['select_option']),
            'checkbox'            => trim($format['checkbox']),
            'dob_day'             => trim($format['dob_day']),
            'dob_month'           => trim($format['dob_month']),
            'dob_year'            => trim($format['dob_year']),
            'radio_input'         => trim($format['radio_input']),
            'multiple_checkboxes' => $multiple_checkboxes,
        ];

        /**
         * Create error/warning messages in case if any of the mandatory fields are empty. Save them in our error/warning array. Create warning message.
         */

        // Validate First Name
        if(empty($form_data['first_name'])) {
            $form_err['first_name'] = true;
        }
        // Validate Email
        if(empty($form_data['email'])) {
            $form_err['email'] = true;
        }
        // Validate Select Option
        if(empty($form_data['select_option'])) {
            $form_err['select_option'] = true;
        }
        // Validate Checkbox
        if(empty($form_data['checkbox'])) {
            $form_data['checkbox'] = 0;
            $form_err['checkbox'] = true;
        }
        // Validate DOB
        if(empty($form_data['dob_day']) || empty($form_data['dob_month']) || empty($form_data['dob_year'])) {
            $form_err['dob_err'] = true;
        }
        if(empty($form_data['dob_day'])) {
            $form_err['dob_day'] = 'error';
        }
        if(empty($form_data['dob_month'])) {
            $form_err['dob_month'] = 'error';
        }
        if(empty($form_data['dob_year'])) {
            $form_err['dob_year'] = 'error';
        }
        // Validate Radio Button
        if(empty($form_data['radio_input'])) {
            $form_err['radio_input'] = true;
        }
        // Validate Categories Remember
        if(empty($form_data['multiple_checkboxes'])){
            $form_err['multiple_checkboxes[]'] = true;
        }

        /**
         *  Before we go into next step make sure that our form is free from errors for that use
         *  Valitron, all validation rules you can find on this link https://github.com/vlucas/valitron
         */

        // Check if form is free of errors.
        $v = new Valitron\Validator($form_data);
        $v->rule('required', ['first_name', 'email']);
        $v->rule('email', 'email');
        // $v->rule('accepted', 'checkbox');
        if($v->validate()) {
            // Success
            // Combine Year Month Day Into Date Format to insert into our database
            $dob = $form_data['dob_year'] . '-' . $form_data['dob_month']. '-' . $form_data['dob_day'];
            // Convert arrays into string different for js validation and php
            if (sizeof($form_data['multiple_checkboxes']) > 1 && !empty($decoded)){
                $multiple_checkboxes = implode(", ", $form_data['multiple_checkboxes']);
            } elseif(empty($decoded)) {
                $multiple_checkboxes = implode(", ", $form_data['multiple_checkboxes']);
            } else {
                $multiple_checkboxes = $form_data['multiple_checkboxes'];
            }

            /**
             * We need to check if that user already exist in our database, to do not multiply record
             */

            // Checking if email been already used
            $query = $dbh->prepare( "SELECT `email` FROM `users` WHERE `email` = ?" );
            $query->bindValue( 1, $form_data['email'] );
            $query->execute();
            // If rows are found for query
            if( $query->rowCount() > 0 ) {

                /**
                 * Update record in our database
                 */

                // If email already exist update data
                $query = $dbh->prepare("UPDATE users SET first_name=:first_name, dob=:dob, radio=:radio, select_option=:select_option, checkbox=:checkbox, multiple_checkboxes=:multiple_checkboxes WHERE email=:email");
                // Bind values

                $query->bindValue(':first_name', $form_data['first_name']);
                $query->bindValue(':dob', $dob);
                $query->bindValue(':radio', $form_data['radio_input']);
                $query->bindValue(':select_option', $form_data['select_option']);
                $query->bindValue(':checkbox', $form_data['checkbox']);
                $query->bindValue(':multiple_checkboxes', $multiple_checkboxes);
                $query->bindValue(':email', $form_data['email']);

                // Execute
                if($query->execute()) {
                    // Reset data and display success message
                    $form_err = [
                        'warning'   => false,
                        'success'   => true
                    ];
                    echo json_encode($form_err);

                } else {
                    return false;
                }

            } else {
                // If email do not exist already in database insert data
                $query = $dbh->prepare("INSERT INTO users (first_name, email, dob, radio, select_option, checkbox, multiple_checkboxes, created_at) VALUES(:first_name, :email, :dob, :radio, :select_option, :checkbox, :multiple_checkboxes, :created_at)");

                // Bind values
                $query->bindValue(':first_name', $form_data['first_name']);
                $query->bindValue(':email', $form_data['email']);
                $query->bindValue(':dob', $dob);
                $query->bindValue(':radio', $form_data['radio_input']);
                $query->bindValue(':select_option', $form_data['select_option']);
                $query->bindValue(':checkbox', $form_data['checkbox']);
                $query->bindValue(':multiple_checkboxes', $multiple_checkboxes);
                $query->bindValue(':created_at', date("Y-m-d H:i:s"));

                if($query->execute()) {
                    // Reset data
                    $form_err = array(
                        'warning'   => false,
                        'success'  => true
                    );
                    echo json_encode($form_err);
                } else {
                    return false;
                }
            }
        } else {
            echo json_encode($form_err);
        }
    }
