<?php

    $dbh;
    $error;

    $dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
    $options = array(
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    );

    // Create PDO instance
    try{
        $dbh = new PDO($dsn, $user, $password, $options);
    }catch(PDOException $e) {
        $error = $e->getMessage();
        echo $error;
    }
    