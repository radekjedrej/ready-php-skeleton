<?php
namespace Utils;

/**
 * get_node_env
 *
 * @return [string] Returns the current NODE_ENV value as set in the relavent .env file
 */
function get_node_env() {
    return getenv('NODE_ENV') ?? "";
}


/**
 * get_project_dir
 * @param  string $path Optional path to a file or directory
 * @return string       Your path from the root of the project
 */
function get_project_dir($path = '') {
    return $_SERVER['DOCUMENT_ROOT'] . '/' . $path;
}


/**
 * check_asset_manifest
 */
function check_asset_manifest() {
    return ( file_exists("assets-manifest.php") ) || false;
}


/**
 * NB LOAD TEMPLATE PART
 *
 * loads a template part using output buffering where you can
 * optionally include $data to be passed into template
 *
 * @param  string $template_name name of the template to be located
 * @param  array  $data          data to be passed into the template to be included
 */
function nb_load_template_part( $template_name, $data=array() ) {

    if ( !strpos($template_name, '.php') ) {
            $template_name = $template_name . '.php';
    }

    // Optionally provided an assoc array of data to pass to tempalte
    // and it will be extracted into variables
    if ( is_array( $data ) ) {
            extract($data);
    }

    ob_start();
    include $template_name;
    $var = ob_get_contents();
    ob_end_clean();

    return $var;
}



/**
 * SLUGIFY
 *
 * @param   string  $string     String to slugify
 * @param   array   $replace    Character to replace
 * @param   string  $delimiter  Character to replace with
 *
 * @return  string              Your string slugify
 */
function slugify($string, $replace = array(), $delimiter = '-') {
  
    if (!empty($replace)) {
        $clean = str_replace((array) $replace, ' ', $clean);
    }
    
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower($clean);
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    $clean = trim($clean, $delimiter);
    
    return $clean;
}