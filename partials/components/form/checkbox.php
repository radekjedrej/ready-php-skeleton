<?php
    $moidifier      = ( !empty($moidifier) ) ? $moidifier : "";
    $name           = ( !empty($name) ) ? $name : "";
    $value          = ( !empty($value) ) ? $value : "";
    $return_value   = ( !empty($return_value) ) ? "checked='checked'" : "";
    $err_message    = ( !empty($err_message) ) ? $err_message : "";
    $label          = ( !empty($label) ) ? $label : "";
    $required       = ( !empty($required) ) ? 'form__input-container--required' : "";
    $aria_required  = ( !empty($required) ) ? "aria-required='true'" : "";
    $err_class      = ( $aria_invalid) ? "form__input-container--error" : "";
    $aria_invalid   = ( $aria_invalid ) ? "aria-invalid='true'" : "aria-invalid='false'";
    $aria           = 'aria_message_';
    $aria_id        = ( isset($err_message) ) ? "id='" . $aria . $name . "'" : "";
    $aria_desc      = ( isset($err_message) ) ? "aria-describedby='" . $aria . $name . "'" : "";
?>




<?php if ($name): ?>
    <div class="form__input-container form__label <?=$moidifier?> <?=$err_class?> <?=$required?>">
        <label class="form__label form-custom-element form-custom-element--checkbox" for="<?=$name?>"><?=$label?>
            <input class="form-custom-element__input js-form-input" type="checkbox" id="<?=$name?>" name="<?=$name?>" value="<?=$value?>" <?=$return_value?> <?=$aria_desc?> <?=$aria_required?> <?=$aria_invalid?>>
            <span class="form-custom-element__indicator"></span>
        </label>
    </div><!-- ./form__input-container ends-->
    <?php if(!empty($err_message)): 
        echo Utils\nb_load_template_part('partials/components/form/error-field', array(
            'err_message'  => $err_message,
            'aria_id'      => $aria_id
        )); ?>
    <?php endif; ?>
<?php endif; ?>