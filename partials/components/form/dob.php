<?php
    $moidifier         = ( !empty($moidifier) ) ? $moidifier : "";
    $dob_day_label     = ( !empty($dob_day_label) ) ? $dob_day_label : "Day";
    $dob_month_label   = ( !empty($dob_month_label) ) ? $dob_month_label : "Month";
    $dob_year_label    = ( !empty($dob_year_label) ) ? $dob_year_label : "Year";
    $dob_day_return    = ( !empty($dob_day_return) ) ? "value='" . $dob_day_return . "'" : "";
    $dob_month_return  = ( !empty($dob_month_return) ) ? "value='" . $dob_month_return . "'" : "";
    $dob_year_return   = ( !empty($dob_year_return) ) ? "value='" . $dob_year_return . "'" : "";
    $legend            = ( !empty($legend) ) ? $legend : "";
    $example           = ( !empty($example) ) ? $example : "E.g. 08 09 1987";
    $required          = ( !empty($required) ) ? 'form__input-container--required' : "";
    $aria_required     = ( !empty($required) ) ? "aria-required='true'" : "";
    // $html_required     = ( !empty($required) ) ? "required" : "";
    $err_message       = ( !empty($err_message) ) ? $err_message : "";
    $day_err_message   = ( !empty($day_err_message) ) ? 'form-dob--error-day' : "";
    $month_err_message = ( !empty($month_err_message) ) ? 'form-dob--error-month' : "";
    $year_err_message  = ( !empty($year_err_message) ) ? 'form-dob--error-year' : "";
    $err_class         = ( $aria_invalid) ? "form__input-container--error" : "";
    $aria_invalid      = ( $aria_invalid ) ? "aria-invalid='true'" : "aria-invalid='false'";
    $aria              = 'aria_message_';
    $aria_id           = ( isset($err_message) ) ? "id='" . $aria . $dob_day_label . "'" : "";
    $aria_desc         = ( isset($err_message) ) ? "aria-describedby='" . $aria . $dob_day_label . "'" : "";
    $aria_label        = 'aria_label_';
    $aria_label_id     = ( isset($err_message) ) ? "id='" . $aria_label . $dob_day_label . "'" : "";
    $aria_label_desc   = ( isset($err_message) ) ? "aria-describedby='" . $aria_label . $dob_day_label . "'" : "";

?>




<?php if (!empty($dob_day_label) && !empty($dob_month_label) && !empty($dob_year_label)): ?>
    <div class="form__input-container form-dob <?=$moidifier?> <?=$required?> <?=$err_class?> <?=$day_err_message?> <?=$month_err_message?> <?=$year_err_message?> " role="group">
        <span class="form__label" <?=$aria_label_id?>>
            <?=$legend?>
            <br>
            <span><?=$example?></span>
        </span>

        <div id="dob_err" class="form-dob__list js-form-input" name="dob_err" <?=$aria_required?>>
            <div class="form-dob__item form-dob__item--day">
                <label class="form-dob__label" for="dob_day" <?=$aria_label_desc?>><?=$dob_day_label?></label>
                <input class="form-dob__input " type="number" min="1" max="31" id="dob-day" type="text" name="dob_day" <?=$dob_day_return?> <?=$aria_desc?>  <?=$html_required?> <?=$aria_invalid?>>
            </div>
            <div class="form-dob__item form-dob__item--month">
                <label class="form-dob__label" for="dob_month" <?=$aria_label_desc?>><?=$dob_month_label?></label>
                <input class="form-dob__input" type="number" min="1" max="12" id="dob-month" type="text" name="dob_month" <?=$dob_month_return?> <?=$aria_desc?> <?=$aria_required?> <?=$html_required?>>
            </div>
            <div class="form-dob__item form-dob__item--year">
                <label class="form-dob__label" for="dob_year" <?=$aria_label_desc?>><?=$dob_year_label?></label>
                <input class="form-dob__input" type="number" min="1920" max="<?=date("Y")?>" id="dob-year" type="text" name="dob_year" <?=$dob_year_return?> <?=$aria_desc?> <?=$aria_required?> <?=$html_required?>>
            </div>
        </div>
    </div>

    <?php if(!empty($err_message)): 
        echo Utils\nb_load_template_part('partials/components/form/error-field', array(
            'err_message'  => $err_message,
            'aria_id'   => $aria_id
        )); ?>
    <?php endif; ?>
<?php endif; ?>