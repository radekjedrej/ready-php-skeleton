<?php
    $return_value   = ( !empty($return_value) ) ? $return_value : "";
    $err_message    = ( !empty($err_message) ) ? $err_message : "";
    $aria_invalid   = ( !empty($aria_invalid) ) ? $aria_invalid : "";
?>




<?php 
    echo Utils\nb_load_template_part('partials/components/form/input', array(
        'moidifier'        => 'select__modifier',
        'name'             => 'email',
        'return_value'     => $return_value,
        'err_message'      => $err_message,
        'aria_invalid'     => $aria_invalid,
        'label'            => 'Email Address',
        'placeholder'      => 'Email Address',
        'required'         => true
    )); 
    ?>