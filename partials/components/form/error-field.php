<?php
    $moidifier   = ( !empty($moidifier) ) ? $moidifier : "";
    $err_message = ( !empty($err_message) ) ? $err_message : "";
    $aria_id     = ( !empty($aria_id) ) ? $aria_id : "";
?>




<div class="form__err-message <?=$moidifier?>" <?=$aria_id?>>
    <?=$err_message?>
</div>