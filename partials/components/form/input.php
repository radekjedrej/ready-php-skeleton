<?php
    $moidifier      = ( !empty($moidifier) ) ? $moidifier : "";
    $name           = ( !empty($name) ) ? $name : "";
    $return_value   = ( !empty($return_value) ) ? "value='" . $return_value . "'" : "";
    $err_message    = ( !empty($err_message) ) ? $err_message : "";
    $type           = ( !empty($type) ) ? $type : "text";
    $placeholder    = ( !empty($placeholder) ) ? "placeholder='" . $placeholder . "'" : "";
    $label          = ( !empty($label) ) ? $label : "";
    $required       = ( !empty($required) ) ? "form__input-container--required" : ""; 
    $aria_required  = ( !empty($required) ) ? "aria-required='true'" : "";
    $err_class      = ( $aria_invalid) ? "form__input-container--error" : "";
    $aria_invalid   = ( $aria_invalid ) ? "aria-invalid='true'" : "aria-invalid='false'";
    $aria           = 'aria_message_';
    $aria_id        = ( isset($err_message) ) ? "id='" . $aria . $name . "'" : "";
    $aria_desc      = ( isset($err_message) ) ? "aria-describedby='" . $aria . $name . "'" : "";

?>




<?php if ($name): ?>
    <div class="form__input-container form__label <?=$moidifier?> <?=$err_class?> <?=$required?>">
        <label class="form__label" for="<?=$name?>"><?=$label?></label>
            <input class="form__input js-form-input" type="<?=$type?>" id="<?=$name?>" name="<?=$name?>" <?=$placeholder?> <?=$return_value?> <?=$aria_desc?> <?=$aria_required?> <?=$aria_invalid?>>
        
    </div><!-- ./form__input-container ends-->

    <?php if(!empty($err_message)): ?>
        <?= Utils\nb_load_template_part('partials/components/form/error-field', array(
            'err_message' => $err_message,
            'aria_id'  => $aria_id
        )); ?>
    <?php endif; ?>
<?php endif; ?>