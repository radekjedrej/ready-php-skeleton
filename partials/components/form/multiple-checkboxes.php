<?php
    $moidifier         = ( !empty($moidifier) ) ? $moidifier : "";
    $name              = ( !empty($name) ) ? $name : "";
    $name_array        = ( !empty($name_array) ) ? $name_array : "";
    $return_value      = ( !empty($return_value) ) ? $return_value : [];
    $err_message       = ( !empty($err_message) ) ? $err_message : "";
    $required          = ( !empty($required) ) ? 'form__input-container--required' : "";
    $aria_required     = ( !empty($required) ) ? "aria-required='true'" : "";
    $err_class         = ( $aria_invalid) ? "form__input-container--error" : "";
    $aria_invalid      = ( $aria_invalid ) ? "aria-invalid='true'" : "aria-invalid='false'";  
    $checkboxes        = ( !empty($checkboxes) ) ? $checkboxes : [];  
    $legend            = ( !empty($legend) ) ? $legend : "";
    $aria              = 'aria_message_';
    $aria_id           = ( isset($err_message) ) ? "id='" . $aria . $name . "'" : "";
    $aria_desc         = ( isset($err_message) ) ? "aria-describedby='" . $aria . $name . "'" : "";
    $aria_label        = 'aria_label_';
    $aria_label_id     = ( isset($err_message) ) ? "id='" . $aria_label . $name . "'" : "";
    $aria_label_desc   = ( isset($err_message) ) ? "aria-describedby='" . $aria_label . $name . "'" : "";
?>



<?php if ($name): ?>
    <div class="form__input-container form__label <?=$moidifier?> <?=$err_class?> <?=$required?>" role="group">
        <span class="form__label" <?=$aria_label_id?>>
            <?=$legend?>
        </span>
        
        <?php foreach ($checkboxes as $checkbox): ?>
            <?php $checked = (in_array($checkbox['value'], $return_value) ? ' checked="checked"' : ''); ?>

            <label class="form-custom-element form-custom-element--checkbox" for="<?=$name?>-<?=$checkbox['value']?>" <?=$aria_label_desc?>><?=$checkbox['label']?>
                <input class="form-custom-element__input js-form-input" type="checkbox" id="<?=$name?>-<?=$checkbox['value']?>" name="<?=$name_array?>" value="<?=$checkbox['value']?>" <?=$checked?> <?=$aria_desc?> <?=$aria_required?> <?=$aria_invalid?>>
                <span class="form-custom-element__indicator"></span>
            </label>
        <?php endforeach; ?>
        
    </div>
    <?php if(!empty($err_message)): 
        echo Utils\nb_load_template_part('partials/components/form/error-field', array(
            'err_message'  => $err_message,
            'aria_id'   => $aria_id
        )); ?>
    <?php endif; ?>
<?php endif; ?>
