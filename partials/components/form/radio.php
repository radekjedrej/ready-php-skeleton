<?php
    $moidifier         = ( !empty($moidifier) ) ? $moidifier : "";
    $radio_button      = ( !empty($radio_button) ) ? $radio_button : [];
    $name              = ( !empty($name) ) ? $name : "";
    $return_value      = ( !empty($return_value) ) ? $return_value : "";
    $err_message       = ( !empty($err_message) ) ? $err_message : "";
    $err_class         = ( $aria_invalid) ? "form__input-container--error" : "";
    $aria_invalid      = ( $aria_invalid ) ? "aria-invalid='true'" : "aria-invalid='false'";
    $required          = ( !empty($required) ) ? 'form__input-container--required' : "";   
    $aria_required     = ( !empty($required) ) ? "aria-required='true'" : "";
    $legend            = ( !empty($legend) ) ? $legend : "";
    $aria              = 'aria_message_';
    $aria_id           = ( isset($err_message) ) ? "id='" . $aria . $name . "'" : "";
    $aria_desc         = ( isset($err_message) ) ? "aria-describedby='" . $aria . $name . "'" : "";
    $aria_label        = 'aria_label_';
    $aria_label_id     = ( isset($err_message) ) ? "id='" . $aria_label . $name . "'" : "";
    $aria_label_desc   = ( isset($err_message) ) ? "aria-describedby='" . $aria_label . $name . "'" : ""; 
?>





<?php if ($radio_button): ?>
    <div class="form__input-container form__label <?=$moidifier?> <?=$err_class?> <?=$required?>" role="group">
        <span class="form__label" <?=$aria_label_id?>>
            <?=$legend?>
        </span>

        <?php foreach ($radio_button as $radio): ?> 
            <?php $checked = ($radio['value'] === $return_value ? ' checked="checked"' : '') ?>  

            <label class="form-custom-element form-custom-element--radio" for="<?=$radio['value']?>" <?=$aria_label_desc?>><?=$radio['label']?> 
                <input class="form-custom-element__input js-form-input" id="<?=$radio['value']?>" type="radio" name="<?=$name?>" value="<?=$radio['value']?>" <?=$checked?> <?=$aria_desc?> <?=$aria_required?> <?=$aria_invalid?>>
                <span class="form-custom-element__indicator"></span>
            </label> 
        <?php endforeach; ?>
    </div>
    <?php if(!empty($err_message)): 
        echo Utils\nb_load_template_part('partials/components/form/error-field', array(
            'err_message'  => $err_message,
            'aria_id'   => $aria_id
        )); ?>
    <?php endif; ?>
<?php endif; ?>