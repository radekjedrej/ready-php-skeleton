<?php
    $moidifier      = ( !empty($moidifier) ) ? $moidifier : "";
    $name           = ( !empty($name) ) ? $name : "";
    $return_value   = ( !empty($return_value) ) ? $return_value : "";
    $err_message    = ( !empty($err_message) ) ? $err_message : "";
    $label          = ( !empty($label) ) ? $label : "";
    $options        = ( !empty($options) ) ? $options : [];
    $required       = ( !empty($required) ) ? 'form__input-container--required' : "";
    $aria_required  = ( !empty($required) ) ? "aria-required='true'" : "";
    $err_class      = ( $aria_invalid) ? "form__input-container--error" : "";
    $aria_invalid   = ( $aria_invalid ) ? "aria-invalid='true'" : "aria-invalid='false'";
    $aria           = 'aria_message_';
    $aria_id        = ( isset($err_message) ) ? "id='" . $aria . $name . "'" : "";
    $aria_desc      = ( isset($err_message) ) ? "aria-describedby='" . $aria . $name . "'" : "";

?>




<?php if ($name): ?>
    <div class="form__input-container form__label <?=$moidifier?> <?=$err_class?> <?=$required?>">
        <label class="form__label" for="<?=$name?>"><?=$label?></label>
        <select class="form__input form__input--select js-form-input" id="<?=$name?>" name="<?=$name?>" <?=$aria_invalid?>>
            <?php foreach ($options as $option): ?>
                <?php $selected = ($option['value'] === $return_value ? 'selected' : '') ?>  

                <option value="<?=$option['value']?>" <?=$aria_desc?> <?=$aria_required?> <?=$selected?>><?=$option['option']?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <?php if(!empty($err_message)): 
        echo Utils\nb_load_template_part('partials/components/form/error-field', array(
            'err_message'     => $err_message,
            'aria_id'      => $aria_id
        )); ?>
    <?php endif; ?>
<?php endif; ?>