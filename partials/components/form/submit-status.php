<?php

$warning          = ( isset($warning) ) ? $warning : "";
$success          = ( isset($success) ) ? $success : "";

$display_warning  = ( $warning === 'warning') ? "form__input-container--message" : "";
$display_success  = ( $success) ? "form__input-container--message" : "";

$success_message  = ( !empty($success_message) ) ? $success_message : "";
$warning_message  = ( !empty($warning_message) ) ? $warning_message : "";

?>
    
<div class="<?=$display_warning?> js-warning-form">
    <!-- Display warning message if errors -->
    <?php if($warning_message): ?>
        <div class='form-warning form__err-message'>
        <?=$warning_message?>
        </div>
    <?php endif; ?>
</div>

<div class="<?=$display_success?> js-success-form">
    <!-- Display success message if success -->
    <?php if($success_message): ?>
        <div class='form-success form__err-message'>
            <?=$success_message?>
        </div>
    <?php endif; ?>
</div>