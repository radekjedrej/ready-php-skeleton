<?php
    $form_data      = ( !empty($form_data) ) ? $form_data : [];
    $form_err       = ( !empty($form_err) ) ? $form_err : [];
?>

<form class='js-skeleton-form' action="<?= $_SERVER['PHP_SELF']; ?>" method="post">

<?php
/**
 * Form Template
 *
 * @return  Input
 *
 * Setting:
 *  'modifier'      => add classes to main wrapper
 *  'name'          => name which will be used in php validation ex: 'first_name' as well it adds id to your input element, and for='name' to your label
 *  'return_value'  => save data in case if any errors in form ex: $form_data['first_name']
 *  'err_message'      => used in case if field is required ex: $form_err['first_err_message']
 *  'label'         => label your field with text
 *  'required'      => insert asterisk next to label '*' value: true of false
 *  'placeholder    => placeholder
 *
 */
?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/submit-status', array(
            'success' => $form_err['success'],
            'warning' => $form_err['warning'],
            'success_message' => 'Your form has been submitted successfully.',
            'warning_message' => 'Please make sure all required fields are complete.'
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/input', array(
            'moidifier'        => 'select__modifier',
            'name'             => 'first_name',
            'return_value'     => $form_data['first_name'],
            'aria_invalid'     => $form_err['first_name'],
            'err_message'      => 'Please enter first name',
            'type'             => 'text',
            'label'            => 'First Name',
            'placeholder'      => 'First Name',
            'required'         => true
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/email', array(
            'moidifier'        => 'select__modifier',
            'name'             => 'email',
            'return_value'     => $form_data['email'],
            'aria_invalid'     => $form_err['email'],
            'err_message'      => 'Please enter your email',
            'label'            => 'Email Address',
            'placeholder'      => 'Email Address',
            'required'         => true
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/select', array(
            'moidifier'       => 'select__modifier',
            'name'            => 'select_option',
            'return_value'    => $form_data['select_option'],
            'aria_invalid'    => $form_err['select_option'],
            'err_message'     => 'Please select option',
            'label'           => 'Select One',
            'required'        => true,
            'options'         => array(
                array(
                    'option' => 'city',
                    'value'  => ''
                ),
                array(
                    'option' => 'UK',
                    'value'  => 'UK'
                ),
                array(
                    'option' => 'Poland',
                    'value'  => 'Poland'
                ),
                array(
                    'option' => 'Papua New Guinea',
                    'value'  => 'Papua New Guinea'
                )
            )
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/checkbox', array(
            'moidifier'    => 'select__modifier',
            'name'         => 'checkbox',
            'return_value' => $form_data['checkbox'],
            'aria_invalid' => $form_err['checkbox'],
            'err_message'  => 'Please select option',
            'label'        => 'Checkbox',
            'required'     => true,
            'value'        => '1'
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/dob', array(
            // 'moidifier'         => 'select__modifier',
            'dob_day_return'    => $form_data['dob_day'],
            'dob_month_return'  => $form_data['dob_month'],
            'dob_year_return'   => $form_data['dob_year'],
            'aria_invalid'      => $form_err['dob_err'],
            'err_message'       => 'Choose date of birth',
            'day_err_message'   => $form_err['day_err_message'],
            'month_err_message' => $form_err['month_err_message'],
            'year_err_message'  => $form_err['year_err_message'],

            'dob_day_label'     => 'Day',
            'dob_month_label'   => 'Month',
            'dob_year_label'    => 'Year',
            'legend'            => 'Birthday',
            'required'          => true
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/radio', array(
            'moidifier'     => 'select__modifier',
            'name'          => 'radio_input',
            'return_value'  => $form_data['radio_input'],
            'aria_invalid'  => $form_err['radio_input'],
            'err_message'   => 'Choose One',
            'required'      => true,
            'legend'        => 'Select Radio Button',


            'radio_button'  => array(
                array(
                    'value' => 'radio_1',
                    'label' => 'Radio 1'
                ),
                array(
                    'value' => 'radio_2',
                    'label' => 'Radio 2'
                ),
            )
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/multiple-checkboxes', array(
            'moidifier'     => 'select__modifier',
            'name'          => 'multiple_checkboxes',
            'name_array'    => 'multiple_checkboxes[]',
            'return_value'  => $form_data['multiple_checkboxes'],
            'aria_invalid'  => $form_err['multiple_checkboxes[]'],
            'err_message'   => 'Please select at least one',
            'required'      => true,
            'legend'        => 'Select Checkboxes',

            'checkboxes'  => array(
                array(
                    'value' => 'multiple_checkbox_1',
                    'label' => 'Checkbox 1'
                ),
                array(
                    'value' => 'multiple_checkbox_2',
                    'label' => 'Checkbox 2'
                ),
                array(
                    'value' => 'multiple_checkbox_3',
                    'label' => 'Checkbox 3'
                )
            )
        ));
        ?>

    <?php
        echo Utils\nb_load_template_part('partials/components/form/submit', array(
            'value' => 'Enter Now'
        ));
        ?>

</form>
