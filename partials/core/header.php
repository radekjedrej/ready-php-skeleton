<?php $heading = $heading ?? ""; ?>

<header class="container band band--double">
    <a href="#main-content" class="skip-link">Skip to content</a>

    <?php if( !empty($heading) ) : ?>
        <h1 class="heading--alpha heading--center">
            <?= $heading ?>
        </h1>
    <?php endif; ?>
</header>
